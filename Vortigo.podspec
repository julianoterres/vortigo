Pod::Spec.new do |s|
  s.name = 'Vortigo'
  s.version = '1.0.0'
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.summary = 'Functions most used to Vortigo'
  s.homepage = 'http://vortigo.com.br'
  s.authors = { "Vortigo" => "contato@vortigo.com.br" }
  #s.source = { :git => "https://gitlab.com/julianoterres/vortigo", :tag  => "v"+s.version.to_s }
  s.source = { :git => "https://gitlab.com/julianoterres/vortigo", :branch  => "master" }
  s.platforms = { :ios => "9.0", :osx => "10.10", :tvos => "9.0", :watchos => "2.0" }
  s.requires_arc = true
  s.swift_version = '5.0'
  s.cocoapods_version = '>= 1.4.0'

  s.default_subspec = "Core"
  s.subspec "Core" do |ss|
    ss.source_files  = "Sources/**/*.swift"
    ss.framework  = "Foundation"
 end
end
