# UIViewExtensions

## Border

Metódo que adiciona borda na view, podendo setar cor e espesura da borda.

```
func border(color: CGColor, width: CGFloat)
```

* **`color`** - Cor que você deseja que a borda tenha.
* **`width`** - Espesura que você deseja que a borda tenha.

```
let view = UIView()
view.border(color: .red, width: 1)
```

## Round

Metódo que transforma uma view em redonda, baseada no seu próprio tamanho.

```
func round()
```

```
let view = UIView()
view.round()
```

Metódo que arredonda uma view, onde você pode setar o tamanho que deseja ser arredondado.

```
func round(radius: CGFloat)
```

* **`radius`** - Raio que você deseja que a view tenha.

```
let view = UIView()
view.round(radius: 10)
```

## Round Corner

Metódo que arredonda somente um dos lados de uma view.

```
func roundCorner(corner: UIRectCorner, radius: CGFloat)
```

* **`corner`** - Canto que você deseja arredondar da view.
* **`radius`** - Raio que você deseja que tenha.

```
let view = UIView()
view.roundCorner(corner: .topLeft, radius: 10)
```

## Semi Circle

Metódo que transforma uma view em um semi círculo.

```
func semiCircle()
```

```
let view = UIView()
view.semiCircle()
```

## Shadow

Metódo que adiciona sombra em uma view.

```
func shadow(opaccity: Float, offSet: CGSize, radius: CGFloat, color: UIColor)
```

* **`opaccity`** - Opacidade que você deseja que a sombra tenha.
* **`offSet`** - Deslocamento que você deseja que a sombra tenha.
* **`radius`** - Raio que você deseja que a sombra tenha.
* **`color`** - Cor que você deseja que a sombra tenha.

```
let view = UIView()
view.shadow(opaccity: 0.5, offSet: CGSize(width: 1, height: 1), radius: 1, color: .black)
```
