# Bem vindo a documentação da biblioteca Vortigo

Esta biblioteca foi desenvolvida para agilizar o seu desenvolvimento.

Nela você encontrara as funções mais usadas no dia a dia, e que poderão ser usadas de forma rápida e fácil.

Você pode instalar ela no seu projeto usando CocoaPods, Carthage ou Swift Package Manager.

Abaixo segue como você pode instalar ela, com cada um dos gerenciadores de dependências citados acima.

## CocoaPods

Adicione no seu arquivo Podfile o comando abaixo

```
pod 'Vortigo', :path => '/Users/julianoterres/iOS/Vortigo/Vortigo'
```

## Carthage

Adicione no seu arquivo Cartfile o comando abaixo

```
git 'https://gitlab.com/julianoterres/vortigo.git' 'master'
```

## Swift Package Manager

Abra seu Xcode, selecione o seu projeto conforme a figura abaixo, e selecione a opção Swift Packages.

![zoomify](./images/add_swift_package.png)

Clique no ícone de adicionar no canto inferior esquerdo. Abrira uma janela onde você deve adicionar a URL abaixo, e após seguir alguns passos para configurar a versão que deseja usar da biblioteca.

```
https://gitlab.com/julianoterres/vortigo.git
```
