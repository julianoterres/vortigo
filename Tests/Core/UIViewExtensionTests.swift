//
//  UIViewExtension.swift
//  Vortigo-iOS
//
//  Created by Juliano Terres on 28/09/19.
//  Copyright © 2019 RahulKatariya. All rights reserved.
//

import Quick
import Nimble
import UIKit
@testable import Vortigo

// MARK: Methods of UIViewExtensionTests
class UIViewExtensionTests: QuickSpec {

  override func spec() {

    var view: UIView!

    describe("UIView") {

      beforeEach {
        view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
      }

      describe("When call border function") {
        beforeEach {
          view.border(color: UIColor.red.cgColor, width: 1)
        }
        it("should the view must have these values ​​in the layer") {
          expect(view.layer.borderColor) == UIColor.red.cgColor
          expect(view.layer.borderWidth) == 1
        }
      }

      describe("When call round function") {
        beforeEach {
          view.round()
        }
        it("should the view must have these values ​​in the layer") {
          expect(view.layer.cornerRadius) == 50
          expect(view.clipsToBounds) == true
        }
      }

      describe("When call round function") {
        beforeEach {
          view.round(corner: 10)
        }
        it("should the view must have these values ​​in the layer") {
          expect(view.layer.cornerRadius) == 10
          expect(view.clipsToBounds) == true
        }
      }

      describe("When call roundCorner function") {
        beforeEach {
          view.roundCorner(corner: .topLeft, radius: 10)
        }
        it("should the view must have these values ​​in the layer") {
          expect(view.layer.mask).notTo(beNil())
          expect(view.layer.masksToBounds) == true
        }
      }
      
      describe("When call semiCircle function") {
        beforeEach {
          view.semiCircle()
        }
        it("should the view must have these values ​​in the layer") {
          expect(view.layer.sublayers?.count)  == 1
          expect(view.backgroundColor) == UIColor.clear
        }
      }

      describe("When call shadow function") {
        beforeEach {
          view.shadowView(opaccity: 1, offSet: CGSize(width: 1, height: 1), radius: 10, color: .red)
        }
        it("should the view must have these values ​​in the layer") {
          expect(view.layer.shadowOpacity) == 1
          expect(view.layer.shadowOffset) == CGSize(width: 1, height: 1)
          expect(view.layer.shadowRadius) == 10
          expect(view.layer.shadowColor) == UIColor.red.cgColor
          expect(view.clipsToBounds) == false
        }
      }

    }

  }

}
