// swift-tools-version:5.0
//
//  Vortigo.swift
//  Vortigo
//
//  Created by Rahul Katariya on 01/04/19.
//  Copyright © 2019 RahulKatariya. All rights reserved.
//

import PackageDescription

let package = Package(
    name: "Vortigo",
    platforms: [
        .iOS(.v8),
        .macOS(.v10_10),
        .tvOS(.v9),
        .watchOS(.v2)
    ],
    products: [
        .library(
            name: "Vortigo",
            targets: ["Vortigo"]
        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "Vortigo",
            dependencies: [],
            path: "Sources"
        ),
        .testTarget(
            name: "VortigoTests",
            dependencies: ["Vortigo"],
            path: "Tests"
        )
    ],
    swiftLanguageVersions: [.v5]
)
