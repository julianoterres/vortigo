//
//  Vortigo.h
//  Vortigo
//
//  Created by Rahul Katariya on 01/04/19.
//  Copyright © 2019 RahulKatariya. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Vortigo.
FOUNDATION_EXPORT double VortigoVersionNumber;

//! Project version string for Vortigo.
FOUNDATION_EXPORT const unsigned char VortigoVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Vortigo/PublicHeader.h>
