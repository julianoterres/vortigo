//
//  UIViewExtension.swift
//  Vortigo-iOS
//
//  Created by Juliano Terres on 09/09/19.
//  Copyright © 2019 RahulKatariya. All rights reserved.
//

import UIKit

public extension UIView {

  /// Method that adds border
  /// - Parameter color: Color to apply to view
  /// - Parameter width: Width to apply to view
  func border(color: CGColor, width: CGFloat) {
    layer.borderWidth = width
    layer.borderColor = color
  }

  /// Method that creates a round view based on view width
  func round() {
    layer.cornerRadius = frame.size.height / 2
    clipsToBounds = true
  }

  /// Method that rounds the edges of the view based on the entered value.
  /// - Parameter radius: Size that should have round border of view
  func round(radius: CGFloat) {
    layer.cornerRadius = radius
    clipsToBounds = true
  }

  /// Method that adds rounding border only to a specific border
  /// - Parameter corner: Corner you want to round
  /// - Parameter radius: Radius to apply to corner
  func roundCorner(corner: UIRectCorner, radius: CGFloat) {
    let rectShape = CAShapeLayer()
    rectShape.bounds = layer.frame
    rectShape.position = center

    var bounds = layer.bounds
    bounds.size.width = bounds.width
    rectShape.path = UIBezierPath(
      roundedRect: bounds,
      byRoundingCorners: corner,
      cornerRadii: CGSize(width: radius, height: radius)
    ).cgPath

    layer.masksToBounds = true
    layer.mask = rectShape
  }

  /// Method that add  a semicircle into view
  func semiCircle() {
    let circlePath = UIBezierPath(
      arcCenter: CGPoint(x: bounds.size.width / 2, y: (bounds.size.height / 2)),
      radius: (bounds.size.width / 2),
      startAngle: .pi * 2,
      endAngle: .pi,
      clockwise: true
    )

    let semiCirleLayer = CAShapeLayer()
    semiCirleLayer.path = circlePath.cgPath
    semiCirleLayer.fillColor = UIColor.red.cgColor

    layer.insertSublayer(semiCirleLayer, at: 0)
    backgroundColor = UIColor.clear
  }

  /// Method that add shadow
  /// - Parameter opaccity: Opacity to apply to shadow
  /// - Parameter offSet: OffSet to apply to shadow
  /// - Parameter radius: Radius to apply to shadow
  /// - Parameter color: Color to apply to shadow
  func shadow(opaccity: Float, offSet: CGSize, radius: CGFloat, color: UIColor) {
    clipsToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opaccity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius 
  }

}
